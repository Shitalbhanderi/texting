<div class="side-nav">
<div class="side-nav-inner">
    <div class="side-nav-logo">
        <a href="index.html">
            <div class="logo logo-dark" style="background-image: url('assets/images/logo/logo.png')"></div>
            <div class="logo logo-white" style="background-image: url('assets/images/logo/logo-white.png')"></div>
        </a>
        <div class="mobile-toggle side-nav-toggle">
            <a href="">
                <i class="ti-arrow-circle-left"></i>
            </a>
        </div>
    </div>
    <ul class="side-nav-menu scrollable">
        <li class="nav-item active">
            <a class="mrg-top-30" href="index.html">
                <span class="icon-holder">
                        <i class="ti-home"></i>
                    </span>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="dropdown-toggle" href="javascript:void(0);">
                <span class="icon-holder">
                        <i class="ti-package"></i>
                    </span>
                <span class="title">Contacts</span>
                <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
            </a>
            <ul class="dropdown-menu {{ request()->is('admin/contact*') ? 'open' : '' }}">
                <li class="{{request()->is('admin/contact') ? 'active' : ''}}">
                    <a href="{{ route('contact')}}">Contacts</a>
                </li>
                <li>
                    <a href="calendar.html">Upload files</a>
                </li>
                <li>
                    <a href="social.html">Copy from file</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="" href="index.html">
                <span class="icon-holder">
                        <i class="ti-home"></i>
                    </span>
                <span class="title">Group</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="" href="index.html">
                <span class="icon-holder">
                        <i class="ti-home"></i>
                    </span>
                <span class="title">Keyword</span>
            </a>
        </li>
    </ul>
</div>
</div>