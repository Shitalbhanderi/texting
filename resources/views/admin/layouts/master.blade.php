<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <title>Espire - Bootstrap 4 Admin Template</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/images/logo/favicon.png">

    <!-- plugins css -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/dist/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/PACE/themes/blue/pace-theme-minimal.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" />

    <!-- page plugins css -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/nvd3/build/nv.d3.min.css') }}" />

    <!-- core css -->
    <link href="{{ asset('assets/css/ei-icon.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

    @stack('page-css')
</head>

<body>
    <div class="app is-collapsed">
        <div class="layout">
            <!-- Side Nav START -->
           @include('admin.layouts.inc.sidebar')
            <!-- Side Nav END -->

            <!-- Page Container START -->
            <div class="page-container">
                <!-- Header START -->
                @include('admin.layouts.inc.header')
                
                <!-- Header END -->

                <!-- Content Wrapper START -->
                <div class="main-content">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
                <!-- Content Wrapper END -->

                <!-- Footer START -->
                @include('admin.layouts.inc.footer')

                <!-- Footer END -->

            </div>
            <!-- Page Container END -->

        </div>
    </div>

    <!-- build:js assets/js/vendor.js -->
    <!-- plugins js -->
    <script src="{{ asset('assets/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/bootstrap/dist/js/bootstrap.js')}}"></script>
    <script src="{{ asset('assets/vendors/PACE/pace.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <!-- endbuild -->

    <!-- page plugins js -->
    <script src="{{ asset('assets/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{ asset('assets/js/maps/jquery-jvectormap-us-aea.js')}}"></script>
    <script src="{{ asset('assets/vendors/d3/d3.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/nvd3/build/nv.d3.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/jquery.sparkline/index.js')}}"></script>
    <script src="{{ asset('assets/vendors/chart.js/dist/Chart.min.js')}}"></script>

    <!-- build:js assets/js/app.min.js -->
    <!-- core js -->
    <script src="{{ asset('assets/js/app.js')}}"></script>
    <!-- endbuild -->

    <!-- page js -->
    <script src="{{ asset('assets/js/dashboard/dashboard.js')}}"></script>

    @stack('page-js')

</body>

</html>